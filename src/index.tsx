import { createRoot } from 'react-dom/client'

import { Application } from '@/components'

const container = document.createElement('div')

container.id = 'root'

document.body.appendChild(container)

createRoot(container).render(<Application />)
