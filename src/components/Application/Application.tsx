import styles from './Application.module.css'

export const Application = () => {
  return (
    <div className={styles.container}>
      <div className={styles.content}>
        <div className={styles.title}>Application</div>
        <a 
          className={styles.link}
          href="http://vk.com/blu_gunderson_tyler"
          target="_blank"
        >
            Тыкать сюда
        </a>
      </div>
    </div>
  )
}
